 <!-- about section start -->
      <section class="about-section">
        <div class="inner-wrap">
        <?php if(get_field('as_text')):?>
          <p class="as-text">
           <?php echo get_field('as_text');?>
          </p>
          <?php endif; ?>
        </div>
      </section>
      <!-- about section end -->