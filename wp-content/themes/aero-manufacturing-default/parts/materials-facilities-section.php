<!-- Materials & Facilities Module start -->
      <section class="materials-facilities-section">
        <div class="mfs-left-section" <?php $mfs_img_one = get_field('mfs_img_one'); if($mfs_img_one) : ?>
        style="background-image:url('<?php echo $mfs_img_one['url']; ?>');"<?php endif; ?>>
          <div class="mfs-left-content" style="">
            <?php if(get_field('mt_title')):?><h2 class="mfs-left-header"><?php echo get_field('mt_title');?></h2><?php endif; ?>
            <?php if(get_field('mt_text')):?><p class="mfs-left-text"><?php echo get_field('mt_text');?></p><?php endif; ?>
           <?php if(get_field('mt_cta_link')):?> <a href="<?php echo get_field('mt_cta_link');?>" class="btn mfs-left-btn"><?php echo get_field('mt_cta_text');?></a><?php endif; ?>
          </div>
        </div>
        <div class="mfs-right-section" <?php $mfs_img = get_field('mfs_img'); if($mfs_img) : ?>
        style="background-image:url('<?php echo $mfs_img['url']; ?>');"<?php endif; ?>>
          <div class="mfs-right-content">
           <?php if(get_field('ft_title')):?> <h2 class="mfs-right-header"><?php echo get_field('ft_title');?></h2><?php endif; ?>
            <?php if(get_field('ft_text')):?><p class="mfs-right-text"><?php echo get_field('ft_text');?></p><?php endif; ?>
           <?php if(get_field('ft_cta_link')):?> <a href="<?php echo get_field('ft_cta_link');?>" class="btn mfs-right-btn"><?php echo get_field('ft_cta_text');?></a><?php endif; ?>
          </div>
        </div>
      </section>
      <!-- Materials & Facilities Module end -->