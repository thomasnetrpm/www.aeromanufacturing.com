<section class="site-intro">
        <div class="inner-wrap">
        <p class="as-text">
           <span>Aero Manufacturing Corp of Massachusetts produces</span> Precision Metal Fabricated and Machined Components for Aircraft, Ground Turbine, Industrial and Commercial Applications.</p>
          <div class="flexslider hero-slider">
            <ul class="slides">
            <?php if( have_rows('hero_slider') ): 
                           
        while ( have_rows('hero_slider') ) : the_row(); ?>
              <li>
              <?php if(get_sub_field('hs_image')):
             $imagelarge = wp_get_attachment_image_src(get_sub_field('hs_image'), 'full');

            ?> 
                <figure class="si-visual">
                  <img src="<?php echo $imagelarge[0]; ?>" alt="<?php echo get_the_title(get_sub_field('hs_image')) ?>" title="<?php echo get_the_title(get_sub_field('hs_image')) ?>" />
           
                </figure>
                 <?php endif; ?>
                <div class="si-content">
                <?php if(get_sub_field('hs_title')):?>
                  <h2 class="si-header">
                   <?php echo get_sub_field('hs_title');?>
                  </h2>
                  <?php endif; ?>
                  <?php if(get_sub_field('hs_text')):?>
                  <p class="si-text">
                   <?php echo get_sub_field('hs_text');?>
                  </p>
                  <?php endif; ?>
                  <?php if(get_sub_field('hs_cta_link')):?><a href="<?php echo get_sub_field('hs_cta_link');?>" class="btn si-cta-1"><?php echo get_sub_field('hs_cta_text');?></a><?php endif; ?>
                </div>
              </li>
              <?php endwhile; ?>
      <?php endif; ?>
            </ul>
          </div>
        </div>
      </section>
    </div>
    <!-- site-header-wrap END -->