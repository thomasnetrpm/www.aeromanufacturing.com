<!--Site Footer-->
    <footer class="site-footer" role="contentinfo">
      <div class="inner-wrap">
        <div class="sf-logo"><a href="/"><img src="<?php bloginfo('template_url');?>/img/footer-logo.png" alt="Footer Logo" title="Footer Logo"></a></div>
        <div class="sf-address">
          <p>
            <strong>AERO Manufacturing Corporation</strong>
            <br> 100 Sam Fonzo Drive - Beverly, MA 01915
          </p>
          <p><span class="sfa-ph">P: <a href="tel:9787201000">978.720.1000</a></span> <span class="sfa-fax">F: 978.720.1050</span>
            <br>E: <a href="mailto:info@aeromanufacturing.com">info@aeromanufacturing.com</a>
          </p>
        </div>
        <div class="sf-links">
        <strong>Links</strong>
          
           <?php wp_nav_menu(array(
      'menu'            => 'Footer Links',
      
    
      'menu_class'      => 'sfl-menu'
     
      )); ?>
         <div class="sf-social">
          <strong>Follow Us</strong>
          <a href="//www.facebook.com/aeromanufacturingcorporation/" target="_blank"><img src="<?php bloginfo('template_url');?>/img/facebook.svg" title="Facebook" alt="Facebook"></a>
        </div>
        </div>
     <div class="footer-iso-logo">
     <a href="#"><img src="<?php bloginfo('template_url');?>/img/Aerospace-and-defence-3.png"></a>
        <a href="#"><img src="<?php bloginfo('template_url');?>/img/Quality-management-2.png"></a>
</div>
      </div>
      <div class="sf-small">
        <div class="inner-wrap">
          <p>&copy; <?php echo date("Y"); ?> <?php bloginfo( 'name' ); ?> All Rights Reserved | Site created by <a href="#" target="_blank">Thomas Marketing Services</a></p>
        </div>
      </div>
    </footer>



