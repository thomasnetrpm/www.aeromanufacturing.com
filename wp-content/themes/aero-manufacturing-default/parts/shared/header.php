<?php Starkers_Utilities::get_template_parts( array( 'parts/shared/search-module' ) ); ?>
<!--Site Header-->
<div class="site-header-wrap">
  <header class="site-header clearfix" role="banner">
    <div class="sh-sticky-wrap">
      <div class="inner-wrap">
        <div class="sh-utility-wrapper">
          <div class="sh-utility-nav">
            <a href="mailto:info@aeromanufacturing.com" class="sh-email">Info@aeromanufacturing</a>
            <a href="tel:9787201000" class="sh-ph">978.720.1000</a>
            <a class="sh-ico-search-mobile search-link " target="_blank" href="#"></a>
          </div>
          <span class="sh-icons">
             
            <a href="#menu" class="sh-ico-menu menu-link"></a>
            </span>
          <a href="/" class="site-logo"><img src="<?php bloginfo('template_url');?>/img/site-logo.svg" alt="Site Logo" title="Site Logo"></a>
        </div>
        <!--Site Nav-->
        <div class="site-nav-container">
          <div class="snc-header">
            <a href="#" class="close-menu menu-link">Close</a>
          </div>
          <span class="snc-btn-section">
              <a href="/rfq" class="snc-rfq">Rfq</a>
              <a class="sh-ico-search search-link" target="_blank" href="#"></a>
            </span>
          <div class="snc-secondary-menu">
            <?php wp_nav_menu(array(
      'menu'            => 'Utility Nav',
      
    
      'menu_class'      => 'snc-menu'
     
      )); ?>
          </div>
          <?php wp_nav_menu(array(
      'menu'            => 'Primary Nav',
      'container'       => 'nav',
      'container_class' => 'site-nav',
      'menu_class'      => 'sn-level-1',
      'walker'        => new themeslug_walker_nav_menu
      )); ?>
        </div>
        <!-- site-nav-container END-->
        <a href="" class="site-nav-container-screen menu-link">&nbsp;</a>
      </div>
      <!--inner-wrap END-->
    </div>
  </header>
