<!-- Mfg. Accreditations section start -->
      <section class="mfg-accreditations-section">
        <div class="inner-wrap">
          
          <?php if(get_field('mas_heading')):?><h2 class="mas-header"><?php echo get_field('mas_heading');?></h2><?php endif; ?>
          
          <?php if(get_field('mas_text')):?><p class="mas-text"><?php echo get_field('mas_text');?></p><?php endif; ?>
          <div class="mas-logo-section">
           <?php if( have_rows('mas_logo_section') ): 
                           
        while ( have_rows('mas_logo_section') ) : the_row(); ?>

        <?php if(get_sub_field('mas_logo_section_image')):
             $imagelarge = wp_get_attachment_image_src(get_sub_field('mas_logo_section_image'), 'full');

            ?> 
            

            <a href="<?php echo get_sub_field('mas_logo_section_link');?>" class="mas-logo-item">
            
            <img src="<?php echo $imagelarge[0]; ?>" alt="<?php echo get_the_title(get_sub_field('mas_logo_section_image')) ?>" title="<?php echo get_the_title(get_sub_field('mas_logo_section_image')) ?>" />
            </a>
            <?php endif; ?>
            <?php endwhile; ?>
              <?php endif; ?>
          </div>
       <?php if(get_field('mas_cta_link')):?>   <a href="<?php echo get_field('mas_cta_link');?>" class="btn mas-btn"><?php echo get_field('mas_cta_text');?></a> <?php endif; ?>
        </div>
      </section>
      <!--Mfg. Accreditations section end -->